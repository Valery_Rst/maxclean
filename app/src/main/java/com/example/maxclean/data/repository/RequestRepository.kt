package com.example.maxclean.data.repository

import com.example.maxclean.data.api.RetrofitClient
import com.example.maxclean.data.models.Request
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RequestRepository {

    suspend fun createRequest(request: Request) {
        val photos: ArrayList<MultipartBody.Part> = ArrayList()
        request.photoFiles?.forEach {
            getMultipartPartBody(it)?.let {file -> photos.add(file)}
        }

        RetrofitClient.instance.createRequest(
            RequestBody.create(MediaType.parse("text/plain"), request.request),
            RequestBody.create(MediaType.parse("audio/mpeg"), request.audioFile),
            RequestBody.create(MediaType.parse("text/plain"), request.name),
            RequestBody.create(MediaType.parse("text/plain"), request.surname),
            RequestBody.create(MediaType.parse("text/plain"), request.mobileNumber),
            photos
        )
    }
    private fun getMultipartPartBody(file: File): MultipartBody.Part? {
        val requestFile: RequestBody =
            RequestBody.create(MediaType.parse("image/jpeg"), file)
        return MultipartBody.Part.createFormData("image/jpeg", file.name, requestFile)
    }
}