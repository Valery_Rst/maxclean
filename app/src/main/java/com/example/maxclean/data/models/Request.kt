package com.example.maxclean.data.models

import java.io.File

data class Request(val request: String? = null,
                   val audioFile: File? = null,
                   val name: String? = null,
                   val surname: String? = null,
                   val mobileNumber: String? = null,
                   val photoFiles: ArrayList<File>? = null)