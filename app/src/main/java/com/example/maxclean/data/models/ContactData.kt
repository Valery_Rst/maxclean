package com.example.maxclean.data.models

data class ContactData(val name: String? = null,
                       val surname: String? = null,
                       val mobileNumber: String? = null)