package com.example.maxclean.data.api

import com.example.maxclean.data.models.Request
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface CleaningApiService {

    @Multipart
    @POST("bids")
    suspend fun createRequest(
        @Part("text_bid") request: RequestBody,
        @Part("name") audioFile: RequestBody,
        @Part("surname") name: RequestBody,
        @Part("phone") surname: RequestBody,
        @Part("photos") mobilePhone: RequestBody,
        @Part photoFiles: ArrayList<MultipartBody.Part>
    ): Response<Request>
}