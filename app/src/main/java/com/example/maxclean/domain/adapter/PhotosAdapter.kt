package com.example.maxclean.domain.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.maxclean.R
import java.io.File
import kotlinx.android.synthetic.main.list_item_photo.view.*

class PhotosAdapter(private val clickListener: (photoFile: File) -> Unit)
    : RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>() {
    private var photoFiles = ArrayList<File>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_photo, parent, false)
        return PhotosViewHolder(view)
    }

    override fun getItemCount(): Int {
        return photoFiles.size
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        Glide.with(holder.itemView.photo.context)
            .load(photoFiles[position].absoluteFile)
            .into(holder.itemView.photo)

        holder.binding(photoFiles[position], clickListener)
    }

    fun setPhotoFiles(photoFiles: ArrayList<File>) {
        this.photoFiles = photoFiles
    }

    inner class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun binding(file: File, clickListener: (photoFile: File) -> Unit) {
            itemView.delete_photo_button.setOnClickListener {
                clickListener(file)
                notifyDataSetChanged()
            }
        }
    }
}