package com.example.maxclean.domain.state

sealed class FormProcessingState<out T> {
    data class Sent<out T>(val value: T): FormProcessingState<T>()
    data class Failure<out T>(val error: Throwable): FormProcessingState<T>()
    data class ValidationError<out T>(val viewResId: Int, val messageResId: Int): FormProcessingState<T>()
    class Progress<out T> : FormProcessingState<T>()
}