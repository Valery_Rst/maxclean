package com.example.maxclean.domain.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.maxclean.data.models.ContactData
import com.example.maxclean.util.ContactDataStorage

class ContactDataViewModel(application: Application) : AndroidViewModel(application) {
    private val preferences = ContactDataStorage(application.applicationContext)
    private val _contactData = MutableLiveData<ContactData>()
    val contactData: LiveData<ContactData>
        get() = _contactData

    init {
        _contactData.value = ContactData(
            preferences.getSavedName(),
            preferences.getSavedSurname(),
            preferences.getSavedMobileNumber()
        )
    }

    fun saveContactData(name: String, surname: String, mobileNumber: String) {
        preferences.storeContactData(name, surname, mobileNumber)
    }
}