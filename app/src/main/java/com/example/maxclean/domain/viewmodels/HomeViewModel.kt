package com.example.maxclean.domain.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.maxclean.R
import com.example.maxclean.data.models.Request
import com.example.maxclean.data.repository.RequestRepository
import com.example.maxclean.domain.state.FormProcessingState
import kotlinx.coroutines.launch
import java.io.File

class HomeViewModel : ViewModel() {
    private val _formState = MutableLiveData<FormProcessingState<Int>>()
    val formState: LiveData<FormProcessingState<Int>>
        get() = _formState
    private val _photoFiles = MutableLiveData<ArrayList<File>>()
    val photoFiles: LiveData<ArrayList<File>>
        get() = _photoFiles
    private lateinit var photoFile: File

    init {
        _photoFiles.value = ArrayList()
    }

    fun requestProcessing(request: String, name: String,
                          surname: String, mobileNumber: String) {
        if (validation(request, name, surname, mobileNumber)) {
            _formState.value = FormProcessingState.Progress()
            sendRequest(Request(
                //TODO вторым параметром необходим аудиофайл
                request, null, name, surname, mobileNumber, _photoFiles.value
            ))
        }
    }

    fun savePhotoFileToArray() {
        _photoFiles.value?.add(photoFile)
    }
    fun deletePhotoFile(file: File) {
        file.delete()
        _photoFiles.value?.remove(file)
    }
    fun getEmptyPhotoFile(storageDir: File): File {
        photoFile = File.createTempFile(PHOTO_FILE_NAME, ".jpeg", storageDir)
        return photoFile
    }

    private fun validation(request: String, name: String,
                           surname: String, mobileNumber: String): Boolean {
        var valid = true
        if (request.isEmpty()) {
            _formState.value = FormProcessingState.ValidationError(
                R.id.request_edit_text, R.string.empty_field_error_tpl
            )
            valid = false
        }
        if (name.isEmpty()) {
            _formState.value = FormProcessingState.ValidationError(
                R.id.name_edit_text, R.string.empty_field_error_tpl
            )
            valid = false
        }
        if (surname.isEmpty()) {
            _formState.value = FormProcessingState.ValidationError(
                R.id.surname_edit_text, R.string.empty_field_error_tpl
            )
            valid = false
        }
        if (mobileNumber.isEmpty()) {
            _formState.value = FormProcessingState.ValidationError(
                R.id.mobile_number_edit_text, R.string.empty_field_error_tpl
            )
            valid = false
        }
        return valid
    }

    private fun sendRequest(request: Request) {
        val repository = RequestRepository()
        viewModelScope.launch {
            try {
                repository.createRequest(request)
                _formState.value = FormProcessingState.Sent(R.string.success)
            } catch (cause: Throwable) {
                _formState.value = FormProcessingState.Failure(cause)
            }
        }
    }

    companion object {
        private const val PHOTO_FILE_NAME = "request"
    }
}