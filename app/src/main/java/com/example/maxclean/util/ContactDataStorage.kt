package com.example.maxclean.util

import android.content.Context
import android.content.SharedPreferences
import com.example.maxclean.R

class ContactDataStorage(context: Context) {
    private val sharedPreferences: SharedPreferences
    private val sharedPrefName: String = context.getString(R.string.pref_contact_data)
    private val keyPersonName: String = context.getString(R.string.pref_key_name)
    private val keyPersonSurname: String = context.getString(R.string.pref_key_surname)
    private val keyPersonPhoneNumber: String = context.getString(R.string.pref_key_phone_number)

    init {
        this.sharedPreferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)
    }

    fun getSavedName(): String {
        return getStringPreferenceByKey(keyPersonName)
    }
    fun getSavedSurname(): String {
        return getStringPreferenceByKey(keyPersonSurname)
    }
    fun getSavedMobileNumber(): String {
        return getStringPreferenceByKey(keyPersonPhoneNumber)
    }

    fun storeContactData(name: String, surname: String, mobileNumber: String) {
        putStringPreference(keyPersonName, name)
        putStringPreference(keyPersonSurname, surname)
        putStringPreference(keyPersonPhoneNumber, mobileNumber)
    }

    private fun putStringPreference(key: String, value: String) {
        with (sharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }
    private fun getStringPreferenceByKey(key: String): String {
        return sharedPreferences.getString(key, "").toString()
    }
}