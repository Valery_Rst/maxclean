package com.example.maxclean.presentation.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import com.example.maxclean.R
import com.example.maxclean.domain.state.FormProcessingState
import com.example.maxclean.domain.adapter.PhotosAdapter
import com.example.maxclean.domain.viewmodels.ContactDataViewModel
import com.example.maxclean.domain.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment(R.layout.fragment_home) {
    private val viewModel: HomeViewModel by lazy {
        ViewModelProvider(this).get(HomeViewModel::class.java)
    }
    private val contactDataViewModel: ContactDataViewModel by lazy {
        ViewModelProvider(this).get(ContactDataViewModel::class.java)
    }
    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                viewModel.savePhotoFileToArray()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("sd", "OK")
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("sd", "OK1")
        val photosAdapter = PhotosAdapter {
                photoFile -> viewModel.deletePhotoFile(photoFile)
        }
        photo_list.apply {
            adapter = photosAdapter
        }

        contactDataViewModel.contactData.observe(viewLifecycleOwner, Observer { data ->
            data.name?.let {name_edit_text.setText(data.name)}
            data.surname?.let {surname_edit_text.setText(data.surname)}
            data.mobileNumber?.let {mobile_number_edit_text.setText(data.mobileNumber)}
        })

        viewModel.photoFiles.observe(viewLifecycleOwner, Observer { photos ->
            photosAdapter.setPhotoFiles(photos)
        })

        viewModel.formState.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is FormProcessingState.ValidationError -> {
                    view.findViewById<EditText>(state.viewResId).apply {
                        error = getString(state.messageResId)
                        requestFocus()
                    }
                }
                is FormProcessingState.Progress -> {
                    contactDataViewModel.saveContactData(
                        name_edit_text.text.toString(),
                        surname_edit_text.text.toString(),
                        mobile_number_edit_text.text.toString()
                    )
                }
                is FormProcessingState.Sent -> {
                    Toast.makeText(context, getString(state.value), Toast.LENGTH_LONG).show()
                }
                is FormProcessingState.Failure -> {
                    Toast.makeText(context, state.error.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        add_photo_button.setOnClickListener {
            cameraResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                val fileUrl = FileProvider.getUriForFile(
                    requireContext(), FILE_PROVIDER_AUTHORITY, viewModel.getEmptyPhotoFile(
                        requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                    )
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUrl)
            })
        }

        send_request_button.setOnClickListener {
            viewModel.requestProcessing(
                request_edit_text.text.toString().trim(),
                name_edit_text.text.toString().trim(),
                surname_edit_text.text.toString().trim(),
                mobile_number_edit_text.text.toString().trim()
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_service_detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.about_service) {
            findNavController(this).navigate(R.id.action_home_to_aboutService)
            return true
        }
        return item.onNavDestinationSelected(findNavController(this))
                || super.onOptionsItemSelected(item)
    }

    companion object {
        private const val FILE_PROVIDER_AUTHORITY = "com.example.maxclean.fileprovider"
    }
}